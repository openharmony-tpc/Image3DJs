# Image3DJs
&emsp;&emsp;最近自如团队实现了一个裸眼3D效果，有点小火，各平台的版本都随之产生了，这怎么少得了鸿蒙的，此组件便是该效果在鸿蒙Js上的实现，感谢自如团队的创意！

## 效果图
<img src="screen/Image3DJs.gif"/>

## 下载安装
```
npm install @ohos/image3djs --save
```
OpenHarmony npm 环境配置等更多内容，请参考参考安装教程 [如何安装 OpenHarmony npm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md)

## 如何使用
1.在你的css中定义你的组件宽高（也可以在hml中通过style来定义）
```css
.image_3d {
    width: 100%;
    height: 460px;
}
```
2.在你的hml中写页面
```hml
<element name="image_3d" src="@ohos/image3djs/src/main/js/components/Image3D/Image3D.hml"></element>
<div class="container">
    <image_3d class="image_3d"
              img-background="/common/images/background.png"
              img-mid="/common/images/mid.png"
              img-foreground="/common/images/foreground.png">
    </image_3d>
    <!--下面须有你自己的布局，否则image3D组件会超过组件边界-->
    <div style="width: 100%; height: 200px; background-color: white;"></div> 
</div>
```

hml中可以使用的自定义属性

| name           | format | description        | default_value  |
| :-------------:| :----: | :----------------: | :------------: |
| scale          | float  | 底图和上层图的放大比例 | 1.2            |
| sensitivity    | float  | 灵敏度              | 1              |
| img-background | string | 底层图片路径         | “ ”            |
| img-mid        | string | 中层图片路径         | “ ”            |
| img-foreground | string | 上层图片路径         | “ ”            |

## 实现原理
&emsp;&emsp;裸眼3D效果，其实就是将图片分为三层（底层、中层、上层），然后将底层与上层进行放大，使其尺寸大于控件尺寸，再监听手机方向传感器，根据手机方向变化来对底层和上层图片进行位移（底层图片的位移方向要与上层图片的位移方向相反哦），中层则不动，以此在视觉上给人一种3D的效果。

## 运行要求
SDK 9+

## LICENSE
```
Copyright (C) 2021 Huawei Device Co., Ltd.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```


